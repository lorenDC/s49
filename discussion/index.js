fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((data) => showPosts(data))


document.querySelector("#form-add-post").addEventListener("submit", (e) => {
	e.preventDefault()

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers: {
			"Content-Type": "application/json"
		}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert("Post Added!")
	})
	document.querySelector("#txt-title").value == null
	document.querySelector("#txt-body").value == null


})


const showPosts = (posts) => {
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
				
    	<div id="post-${post.id}">
    		<h3 id="post-title-${post.id}">${post.title}</h3>
    		<p id="post-body-${post.id}">${post.body}</p>
	    	<button onClick="editPost('${post.id}')">Edit</button>
			<button onClick="deletePost('${post.id}')">Delete</button>

    	</div>
	`
	})
	// console.log(postEntries)
	document.querySelector("#div-post-entries").innerHTML = postEntries
}

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	// Pass the id, title, and body from the post to be updated in the Edit Post Form
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	// removeAttribute() removes the disabled attribute/property.
	document.querySelector("#btn-submit-update").removeAttribute("disabled")
};


// Update post data
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

		e.preventDefault();

		let id = document.querySelector("#txt-edit-id").value;

		fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
			method: "PUT",
			body: JSON.stringify({
				id: id,
				title: document.querySelector("#txt-edit-title").value,
				body: document.querySelector("#txt-edit-body").value,
				userId: 1
			}),
			headers: {
				"Content-Type": "application/json"
			}

		})
		.then((response) => response.json())
		.then((data) => {
			console.log(data);
			alert("Successfully updated!")
		})

		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;

		document.querySelector("#btn-submit-update").setAttribute("disabled", true)
});

//  Activity
const deletePost = (id) => {
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: "DELETE"
	})
	let deletedPost = document.querySelector(`#post-${id}`)
	deletedPost.remove()
	alert(`Post ID: ${id} was removed!`)
}

